#!/bin/bash
#Un script que reemplaza recursivamente un patrón en un directorio o archivo.

FILE=${1:?"Debe proporcionar una ruta a un archivo o carpeta."}

if [ -e $FILE ]
then

        echo "Especificar un  patrón para reemplazar :"
        read PATTERN
        echo "¿Quieres reemplazar este patrón con ?"
        read REPLACE

        if [ -d $FILE ]
        then
                cd $FILE
                find . -type f -print0 | xargs -0 sed -i "s|$PATTERN|$REPLACE|g"
        elif [ -f $FILE ]
        then
                sed -i "s|$PATTERN|$REPLACE|g" $FILE
        else
                echo "$FILE no es ni un directorio ni un simple archivo."
        fi

else
        echo "El archivo o carpeta especificada no existe."
fi
