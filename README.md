# A script to find and replace a pattern in a tree of folders and files

english: A script to find and replace a pattern in a tree of folders and files.
<br/>French: Un script pour chercher et remplacer un motif dans une arborescence de dossiers et de fichiers.

Spanish: Un script para encontrar y reemplazar un patrón en un árbol de carpetas y archivos.

Script développé pour : [https://gitlab.com/garagenum/tutos](https://gitlab.com/garagenum/tutos) 
